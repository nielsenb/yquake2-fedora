================
 yquake2-fedora
================

Nothing but a specfile for building an RPM.

See the official `Yamagi Quake II`_ page for more details.

.. _Yamagi Quake II: http://www.yamagi.org/quake2/
