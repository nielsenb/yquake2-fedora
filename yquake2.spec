Name:		yquake2
Version:	7.43
Release:	1%{?dist}
Summary:	The Yamagi Quake II client for iD software's Quake II
Group:		Games
License:	GPLv2
URL:		http://www.yamagi.org/quake2
Source0:	http://deponie.yamagi.org/quake2/quake2-%{version}.tar.xz
Source1:	%{name}.desktop
#Get rid of rpaths
Patch0:		yquake2-norpath.patch
BuildRequires:	gcc
BuildRequires:	pkgconfig(libcurl)
BuildRequires:	pkgconfig(vorbis)
BuildRequires:	pkgconfig(ogg)
BuildRequires:	pkgconfig(gl)
BuildRequires:	pkgconfig(openal)
BuildRequires:	pkgconfig(sdl2)
BuildRequires:	pkgconfig(zlib)

%description
This is the Yamagi Quake II Client, an enhanced Version of id Software's Quake
II. The main focus is single player, the gameplay and the graphics are
unchanged, but many bugs were fixed. Unlike most other Quake II ports Yamagi
Quake II is full 64 bit clean so it works perfectly on modern amd64 (x86_64)
processors and operating systems. This code should run on Windows XP or later,
Mac OS X 10.6 or higher and on most unix-like operating systems
(only FreeBSD, Linux and OpenBSD are officially supported and tested, for other
systems you'd at least have to edit the Makefile), just type "make" or "gmake"
to compile.

This code is based upon Icculus Quake II, which itself is built upon id
Software's original code drop. Additional code and patches by many contributers
were used. It's released under the terms of the GPL version 2. See the LICENSE
file for further information.

%prep
%setup -q -c -n quake2
cd quake2-%{version}
%patch0 -p1
cd -

%build
cd quake2-%{version}
%make_build WITH_SYSTEMWIDE=yes

%install
#Install the icon
%{__install} -D -p -m 644 quake2-%{version}/stuff/icon/Quake2.png \
	%{buildroot}%{_datadir}/icons/hicolor/512x512/apps/%{name}.png

cd quake2-%{version}
mkdir -p %{buildroot}%{_datarootdir}/games/quake2
cp -a release/q2ded %{buildroot}%{_datarootdir}/games/quake2
cp -a release/quake2 %{buildroot}%{_datarootdir}/games/quake2
cp -a release/baseq2 %{buildroot}%{_datarootdir}/games/quake2

#Symlink the binary
mkdir -p %{buildroot}%{_bindir}
ln -s %{_datarootdir}/games/quake2/quake2 %{buildroot}%{_bindir}

#Install the desktop file
desktop-file-install --dir=%{buildroot}%{_datadir}/applications %{SOURCE1}

%files
%license quake2-%{version}/LICENSE
%{_datarootdir}/icons/hicolor/512x512/apps/%{name}.png
%{_datarootdir}/applications/%{name}.desktop
%{_datarootdir}/games/quake2/baseq2/game.so
%{_datarootdir}/games/quake2/q2ded
%{_datarootdir}/games/quake2/quake2
%{_bindir}/quake2

%changelog
* Fri May 22 2020 Brandon Nielsen <nielsenb@jetfuse.net> 7.43-1
- Add dist tag to release
- Use pkgconfig for BuildRequires

* Fri Jun 21 2019 Brandon Nielsen <nielsenb@jetfuse.net> 7.41-1
- Update to 7.41
- General specfile cleanup
- Add desktop file

* Fri Jun 24 2016 Brandon Nielsen <nielsenb@jetfuse.net> 5.33-1
- Upgrade to 5.33

* Sun Apr 19 2015 Brandon Nielsen <nielsenb@jetfuse.net> 5.30-1
- Initial specfile
